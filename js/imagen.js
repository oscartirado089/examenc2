document.getElementById('imageInput').addEventListener('change', function () {
    const preview = document.getElementById('preview');
    const file = this.files[0];

    if (file) {
        preview.src = URL.createObjectURL(file);
    } else {
        preview.src = '';
    }
});