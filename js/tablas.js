var select = document.getElementById('numeroSelect');

// Llena el combo box con números del 1 al 10
for (var i = 1; i <= 10; i++) {
    var option = document.createElement('option');
    option.text = i;
    option.value = i;
    select.add(option);
}

mostrarBtn.addEventListener('click', function() {

    var numero = parseInt(select.value);

    tablaResultado.innerHTML = '';

    for (var i = 1; i <= 10; i++) {
        var fila = document.createElement('div');
        fila.className = 'fila';

        var imgNumero1 = document.createElement('img');
        imgNumero1.src = '/img/' + numero + '.png';
        imgNumero1.style.width = '20px'; // Ajustar el ancho a 20px
        imgNumero1.style.height = '20px'; // Ajustar el alto a 20px
        fila.appendChild(imgNumero1);

        var imgMultiplicacion = document.createElement('img');
        imgMultiplicacion.src = '/img/x.png';
        imgMultiplicacion.style.width = '20px'; // Ajustar el ancho a 20px
        imgMultiplicacion.style.height = '20px'; // Ajustar el alto a 20px
        fila.appendChild(imgMultiplicacion);

        var imgNumero2 = document.createElement('img');
        imgNumero2.src = '/img/' + i + '.png';
        imgNumero2.style.width = '20px'; // Ajustar el ancho a 20px
        imgNumero2.style.height = '20px'; // Ajustar el alto a 20px
        fila.appendChild(imgNumero2);

        var imgIgual = document.createElement('img');
        imgIgual.src = '/img/=.png';
        imgIgual.style.width = '20px'; // Ajustar el ancho a 20px
        imgIgual.style.height = '20px'; // Ajustar el alto a 20px
        fila.appendChild(imgIgual);

        var resultadoMultiplicacion = numero * i;

        var digitos = resultadoMultiplicacion.toString().split('');

        digitos.forEach(function(digito) {
            var imgDigito = document.createElement('img');
            imgDigito.src = '/img/' + digito + '.png';
            imgDigito.style.width = '20px'; // Ajustar el ancho a 20px
            imgDigito.style.height = '20px'; // Ajustar el alto a 20px
            fila.appendChild(imgDigito);
        });

        tablaResultado.appendChild(fila);

        // Agregar un salto de línea después de cada fila
        var saltoLinea = document.createElement('br');
        tablaResultado.appendChild(saltoLinea);
    }
});
